Fierce_SSO version 1.0 for Drupal 5

Fierce_SSO allows you to log on to multiple Drupal sites at one time, 
even across different domains and no matter which site they're on when
they enter their password.

It works by adding hidden, 1-pixel images to the welcome message that a
user gets after logging in. 

INSTALLATION:

Step 1:
	This module assumes that you have Drupal set up such that the Users
	table is shared across sites. In other words, you should have a 
	$db_prefix set up in the settings.php for each site that looks kinda
	like this:
	  $db_prefix = array(
		'default'        => 'mysitenamehere_',
		'authmap'        => 'shared_',
		'profile_fields' => 'shared_',
		'profile_values' => 'shared_', 
		'role'           => 'shared_',
		'sequences'      => 'shared_',
		'users'          => 'shared_',
		'users_uid_seq'  => 'shared_',
	  );
	
	Important Note: Unlike other SSO solutions, Fierce_SSO requires that
	you DO NOT share the sessions table. By design, each visitor will 
	have a different session for each of your sites.
	
	Author: Eli Dickinson, FierceMarkets -- eli (at) fiercemarkets.com

Step 2:
	If you database is set up, unzip and install this module as normal
	on each of your sites. Then go to:
	 Site Configuration > Single Sign-On (admin/settings/fierce_sso)
	and enter a list of all the domains you wish to use for SSO, one per 
	line. You must do this each site.
	In other words, if you have foo.com and bar.com, you must list both
	foo.com and bar.com in foo's admin screen and in bar's admin screen.

Step 3:
	Set a Shared Secret. This is a password that is used to authenticate
	logins between sites. This should be a *long* random string of letters
	and numbers. IMPORTANT: This secret must be exactly the same on each
	site using Fierce_SSO

Step 4:
	It's optional, but it's a good idea to also set a welcome message
	like "Welcome to Foo.com" on the admin screen. Otherwise, depending
	on your theme, users may see a weird empty box when they log in
	(this box is used to serve the hidden images that make this whole
	 system work).